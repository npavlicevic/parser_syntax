#include "syntax.h"

int main(int argc, char **args) {
  int is_correct_literal_;
  int is_correct_tag_;
  int is_correct_comment_;
  int is_correct_number_;
  int is_correct_real_;
  int line_max_depth_;
  IsCorrect *is_correct;
  int i;
  Line *line;
  Line *line_;
  Table *table;
  FILE *file;
  int buffer_capacity;
  char *buffer;

  is_correct = is_correct_make();
  i = 0;
  line = NULL;
  line_ = NULL;
  table = table_create(2048);
  buffer_capacity = 128;
  buffer = (char*)calloc(buffer_capacity, sizeof(char));

  if(argc != 2) {
    return 1;
  }

  is_correct_compile(is_correct);
  printf("file name %s \n", args[1]);
  file = fopen(args[1], "r");
  for(;1; i++) {
    line = line_read(file, i, buffer, buffer_capacity);

    if(line == NULL) {
      break;
    }

    // DONE TODO remove is_correct_line
    // DONE TODO remove is_correct_indent
    line_trim(line, buffer_capacity);
    line_hash(line);
    is_correct_literal_ = is_correct_literal(is_correct, line);

    is_correct_comment_ = is_correct_comment(is_correct, line);
    if(!is_correct_comment_) {
      line_destroy(&line);
      continue;
    }

    is_correct_tag_ = is_correct_tag(is_correct, line);
    if(!is_correct_tag_) {
      line_cast_tag(line);
    }

    is_correct_number_ = is_correct_number(is_correct, line);
    if(!is_correct_number_) {
      line_ = line_cast_number(line);
    }

    is_correct_real_ = is_correct_real(is_correct, line);
    if(!is_correct_real_) {
      line_ = line_cast_real(line);
    }

    if(!(is_correct_number_ && is_correct_real_)) {
      line_destroy(&line);
      line = line_;
    }

    line_print(line);
    line_print_type(line);
    printf("depth %d ", line->depth);
    printf("is correct literal %d ", is_correct_literal_);
    printf("is correct tag  %d ", is_correct_tag_);
    printf("is correct number %d ", is_correct_number_);
    printf("is correct real %d \n", is_correct_real_);
    table_insert(table, line, hash_mirror_int(&(line->line_number)));
  }

  line_max_depth_ = line_max_depth(table);
  printf("\nmaximum depth %d \n\n", line_max_depth_);
  build_symbol_tree(table, line_max_depth_);
  print_symbol_tree(table, i);

  is_correct_destroy(&is_correct);
  line_table_destroy(table);
  table_destroy(&table);
  fclose(file);
  free(buffer);

  return 0;
}
