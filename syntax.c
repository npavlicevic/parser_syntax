#include "syntax.h"

/*
* Function: is_correct_make
* 
* make syntax rules
*
* return: rules
*/
IsCorrect *is_correct_make() {
  return (IsCorrect*)calloc(1, sizeof(IsCorrect));
}
/*
* Function: is_correct_compile
* 
* compile syntax rules
*
* return: integer
*/
int is_correct_compile(IsCorrect *this) {
  int r;

  r = regcomp(&(this->literal), "^[[:alpha:]]+[[:alnum:][:punct:]]+", REG_EXTENDED);

  if(r) {
    return r;
  }

  r = regcomp(&(this->tag), "^[\\.]{1,1}[[:alnum:]]+", REG_EXTENDED);

  if(r) {
    return r;
  }

  r = regcomp(&(this->comment), "^[\\~]+", REG_EXTENDED);

  if(r) {
    return r;
  }

  r = regcomp(&(this->number), "^[\\-]{0,1}[[:digit:]]+", REG_EXTENDED);

  if(r) {
    return r;
  }

  r = regcomp(&(this->real), "^[\\-]{0,1}[[:digit:]]+[\\.][[:digit:]]+", REG_EXTENDED);

  return r;
}
/*
* Function: is_correct_literal
* 
* check is correct literal
*
* return: integer
*/
int is_correct_literal(IsCorrect *this, Line *line) {
  int r;

  r = regexec(&(this->literal), line->value.text, 0, NULL, 0);

  return r;
}
/*
* Function: is_correct_tag
* 
* check is correct tag
*
* return: integer
*/
int is_correct_tag(IsCorrect *this, Line *line) {
  int r;

  r = regexec(&(this->tag), line->value.text, 0, NULL, 0);

  return r;
}
/*
* Function: is_correct_comment
* 
* check is correct comment
*
* return: integer
*/
int is_correct_comment(IsCorrect *this, Line *line) {
  int r;

  r = regexec(&(this->comment), line->value.text, 0, NULL, 0);

  return r;
}
/*
* Function: is_correct_number
* 
* check is correct number
*
* return: integer
*/
int is_correct_number(IsCorrect *this, Line *line) {
  int r;

  r = regexec(&(this->number), line->value.text, 0, NULL, 0);

  return r;
}
/*
* Function: is_correct_real
* 
* check is correct real
*
* return: integer
*/
int is_correct_real(IsCorrect *this, Line *line) {
  int r;

  r = regexec(&(this->real), line->value.text, 0, NULL, 0);

  return r;
}
/*
* Function: build_symbol_tree
* 
* build symbol tree
*
* return: void
*/
void build_symbol_tree(Table *table, int max_depth) {
  int depth = max_depth;
  int i;
  int j;
  Line *line;
  Line *parent;

  for(;depth >= 0; depth--) {
    i = table->capacity - 1;
    for(;i >= 0; i--) {
      line = (Line*)table->value[i];
      if(line == NULL) {
        continue;
      }

      if(line->depth == depth) {
        j = i - 1;
        for(;j >= 0; j--) {
          parent = (Line*)table->value[j];
          if(parent == NULL) {
            continue;
          }
          if(parent->depth < depth) {
            line->parent = j;
            line->parent_hash = parent->hash;
            break;
          }
        }
      }
    }
  }
}
/*
* Function: print_symbol_tree
* 
* print symbol tree
*
* return: void
*/
void print_symbol_tree(Table *table, int max_lines) {
  int i = 0;
  Line *line;

  for(;i < max_lines; i++) {
    line = (Line*)table->value[i];
    if(line == NULL) {
      printf(" \n");
      continue;
    }

    line_print_all(line);
    printf("\n");
  }
}
/*
* Function: is_correct_destroy
* 
* destroy syntax
*
* return: void
*/
void is_correct_destroy(IsCorrect **this) {
  regfree(&((*this)->literal));
  regfree(&((*this)->tag));
  regfree(&((*this)->comment));
  regfree(&((*this)->number));
  regfree(&((*this)->real));
  free(*this);
  *this = NULL;
}
