#ifndef SYNTAX_H
#define SYNTAX_H
#include <regex.h>
#include "parser_config.h"
typedef struct is_correct {
  regex_t literal;
  regex_t tag;
  regex_t comment;
  regex_t number;
  regex_t real;
} IsCorrect;
IsCorrect *is_correct_make();
int is_correct_compile(IsCorrect *this);
int is_correct_literal(IsCorrect *this, Line *line);
int is_correct_tag(IsCorrect *this, Line *line);
int is_correct_comment(IsCorrect *this, Line *line);
int is_correct_number(IsCorrect *this, Line *line);
int is_correct_real(IsCorrect *this, Line *line);
void build_symbol_tree(Table *table, int max_depth);
void print_symbol_tree(Table *table, int max_lines);
void is_correct_destroy(IsCorrect **this);
#endif
