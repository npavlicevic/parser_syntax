#
# Makefile 
# parser syntax
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic -DMORE
OBJ_FLAGS=-c -static -g
AR_FLAGS=rcs
LIBS=-lm -ltable -lparserconfig

FILES=syntax.h syntax.c syntax_main.c
CLEAN=syntax_main

all: main obj lib

main: ${FILES}
	${CC} ${CFLAGS} syntax.c syntax_main.c ${LIBS} -o syntax_main

obj: ${FILES}
	${CC} ${OBJ_FLAGS} syntax.c -o syntax.o

lib: ${FILES}
	${AR} ${AR_FLAGS} libparsersyntax.a syntax.o

clean: ${CLEAN}
	rm $^
